
<h2> Hi, I'm <a href="https://saugatreemal.engineer">Saugat Rimal</a><img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="25px"></h2>

<br/>
<p align="left"> <img src="https://komarev.com/ghpvc/?username=saugat-rimal&label=Profile Views&color=blue&style=plastic" alt="saugat-rimal" /> </p>

Hi, I'm Saugat Rimal,👨‍💻 **`Full-Stack Developer`** who is exploring Cloud ☁️ while facilitating the world with User Experience with my Design Thinking Skills 🧐 and Enthusiast about Cyber.  I have always loved creating something out of nothing.😉

Working 💼 [**`@Hyburtech`**](https://hyburtech.com/) as a Web Developer.

🤓 I have a keen interest in 🤝 collaborating with others and empowering others to build digital solutions that solve real-world 🌍 problems. I'm also a **`Design Thinking facilitator`** and a **`Creative Technologist`** who believes that the merger between Design Thinking and Digital Technologies will lead to the building of user-centered solutions that are impactful toward the betterment of society.

<img align="right" alt="GIF" src="https://media.giphy.com/media/836HiJc7pgzy8iNXCn/giphy.gif" />
 
### <img src="https://media.giphy.com/media/VgCDAzcKvsR6OM0uWg/giphy.gif" width="50"> A little more about me... 

- 🔭 I’m love exploring🔭 the world🌍 inside the computer💻.
- 🌱 I’m currently learning **Cloud Computing☁️** & **Cyber Security**👨‍💻.
- 👯 I’m looking to collaborate on crafting great **web experiences🤝**.
- 🤔 I’m looking for help with my **Front-End Designing🤙**.
- 💬 Ask me about anything, I am happy😁 to help👯. 
- 😄 Nickname: Dark Demon 🏴‍☠️
- ⚡ Fun fact: I love building🧱 and occasionally **designing✍️** exceptional **digital experiences**.
- 📝 Writings: [You can checkout✔️ some of my awesome🥳 blogs](https://blog.saugatreemal.engineer/)
- ☕ 👇 We can have coffee too 😄
 
 <a href="https://www.buymeacoffee.com/saugatrimal"> <img width="160" src="https://img.shields.io/badge/-%E2%98%95%20Buy%20me%20a%20coffee-fd0?style=flat"> </a>
 
     

<br>

---


### Tech & Tools Preference


![HTML5](https://img.shields.io/badge/-HTML5-black?style=for-the-badge&logo=html5&logoColor=white)
![CSS3](https://img.shields.io/badge/-CSS3-black?style=for-the-badge&logo=css3&logoColor=1572B6)
![Bootstrap](https://img.shields.io/badge/-Bootstrap-black?style=for-the-badge&logo=Bootstrap)
![JavaScript](https://img.shields.io/badge/-JavaScript-black?style=for-the-badge&logo=javascript)
![Typescript](https://img.shields.io/badge/-Nodejs-black?style=for-the-badge&logo=Typescript&logoColor=5df58b)
![JQuery](https://img.shields.io/badge/-JQuery-black?style=for-the-badge&logo=jquery&logoColor=3178C6)
![C & C++](https://img.shields.io/badge/-C%20&%20C++-black?style=for-the-badge&logo=C%20&%20C++)
![Git](https://img.shields.io/badge/-Git-black?style=for-the-badge&logo=Git)    
![Linux](https://img.shields.io/badge/-Linux-black?style=for-the-badge&logo=Linux&logoColor=FCC624)
![VsCode](https://img.shields.io/badge/-VS%20Code-black?style=for-the-badge&logo=visual%20studio%20code&logoColor=white)
![Heroku](https://img.shields.io/badge/-Heroku-black?style=for-the-badge&logo=Heroku&logoColor=1572B6)
![Gitkraken](https://img.shields.io/badge/-Gitkraken-black?style=for-the-badge&logo=gitkraken)
![Sentry](https://img.shields.io/badge/-Sentry-black?style=for-the-badge&logo=sentry)
![Netlify](https://img.shields.io/badge/-netlify-black?style=for-the-badge&logo=netlify)
![Gatsby](https://img.shields.io/badge/-gatsby-black?style=for-the-badge&logo=gatsby)
![Vercel](https://img.shields.io/badge/-Vercel-black?style=for-the-badge&logo=vercel)
![Wordpress](https://img.shields.io/badge/-Wordpress-black?style=for-the-badge&logo=wordpress)
![Woocommerce](https://img.shields.io/badge/-woocommerce-black?style=for-the-badge&logo=woocommerce)
![Ghost](https://img.shields.io/badge/-ghost-black?style=for-the-badge&logo=ghost)


---

### My GitHub Status📈: 

 

[![GitHub stats]( https://github-readme-streak-stats.herokuapp.com/?user=saugat-rimal&theme=radical)](https://github.com/saugat-rimal)
<p>
  <img height="180em" src="https://github-readme-stats.vercel.app/api?username=saugat-rimal&show_icons=true&hide_border=true&&count_private=true&include_all_commits=true&theme=radical" />
  <img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=saugat-rimal&count_private=true&include_all_commits=true&show_icons=true&hide_border=true&hide=html&layout=compact&langs_count=8&theme=radical"/>
</p>


**NOTE:** Top languages does not indicate my skill level or something like that, it's a github metric of which languages i have the most code on github.

 
 
---


### You can find in me in the web 🌍

[![Twitter: saugatrimal60](https://img.shields.io/twitter/follow/saugatrimal60?style=social)](https://twitter.com/saugatrimal60)
[![Hashnode](http://img.shields.io/badge/-Blog-2962ff?style=flat&logo=hashnode&logoColor=white&link=https://blog.saugatreemal.engineer/)](https://blog.saugatreemal.engineer/)
[![Hashnode](https://img.shields.io/badge/-hashnode-darkblue?style=flat&logo=hashnode&logoColor=white)](https://diary.saugatreemal.engineer/)
[![Instagram](http://img.shields.io/badge/-Instagram-E4405F?style=flat&logo=instagram&logoColor=white)](https://www.instagram.com/saugatrimal60/)
[![Linkedin: saugatrimal](https://img.shields.io/badge/-LinkedIn-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/saugatrimal/)](https://www.linkedin.com/in/saugatrimal/)
[![Mail](https://img.shields.io/badge/-Gmail-D14836?style=flat&logo=gmail&logoColor=white)](mailto:saugatrimal60@gmail.com)
[![GitHub saugat-rimal](https://img.shields.io/github/followers/saugat-rimal?label=follow&style=social)](https://github.com/saugat-rimal)
[![Portfolio](http://img.shields.io/badge/-Portfolio%20Website-ffffff?style=flat&logo=data%3Aimage%2Fpng%3Bbase64%2CiVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAAdgAAAHYBTnsmCAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAEYSURBVDiNxdHNK4RRFMfxzzMzhVJeirKwIZKVyG4WY22nrCwoG%2FkHbGYzO%2FkfLKysZSHFgmxtKCJkNTLEyEtZTGPx3KnpaWSS8q3bOffcc37ndC7%2FTYRldKKCdMJ%2Bxwbm8QJ57GMOV5jFaRD5iXyEHZzjCb24D7bYhEAugwOsNpHciCiNa7wlHiYTE%2FSggHEM4CTEsynxMmAME8GfRg6D4f6Kh%2BDf1HdKBTsaio4xhAscYhH96K4Ty2IF64hqAo%2FoQitmsIV2tKCMEs7QFk4ae6jWBEpYwzAy%2BAh%2BIYzfh6nQoBUj2BSUsjjCe5jkUrzUIj7rdvAs%2Fuo7bIu%2F%2BzYTOtaohIQkVew2iC9EWEJHg8dmKP%2By7g%2F5Ahl%2FO9wcY8OAAAAAAElFTkSuQmCC&logoColor=white)](https://saugatreemal.engineer/)
[![Facebook](https://img.shields.io/badge/-Facebook-2962ff?style=flat&logo=facebook&logoColor=white&link=https://www.facebook.com/saugatrimal.pro/)](https://www.facebook.com/saugatrimal.pro/)
[![CodePen](https://img.shields.io/badge/-codepen-black?style=flat&logo=codepen&logoColor=white&link=https://codepen.io/saugatrimal)](https://codepen.io/saugatrimal)



---


<div align="center">  
      
 
Let's connect 👨‍💻 and forge the future together. 😁✌ 

**Check the Repositories and don't forget to give a star.** 👇

<!-- 
Want to give some Credit. Simply uncomment the next line
Github Profile Readme Inspired by [@saugat-rimal](https://github.com/saugat-rimal) 
-->

</div>
